import { HttpStatus, Injectable } from '@nestjs/common';
import { UpdateAuthDto } from './dto/update-auth.dto';
import { SingupAuthDto } from './dto/sing-up.dto';
import { AuthEntity } from './entities/auth.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { IResponse } from 'src/interfaces/type-response';
import { returnResponse } from 'src/shared/return-response';
import { MESSAGES } from 'src/constants/message';
import { decrypt, encrypt } from 'src/shared';
import { JwtService } from '@nestjs/jwt';
import { SinginAuthDto } from './dto/sing-in.dto';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(AuthEntity)
    private readonly authRepo: Repository<AuthEntity>,
    private jwtService: JwtService,
  ) {}

  async singup(singupAuthDto: SingupAuthDto): Promise<IResponse> {
    try {
      const authModel = new AuthEntity();

      authModel.username = singupAuthDto.username;
      authModel.password = encrypt(singupAuthDto.password);

      const saveAuth = await this.authRepo.save(authModel);

      if (saveAuth) {
        return returnResponse(true, HttpStatus.CREATED, MESSAGES.SUCCESS);
      } else {
        return returnResponse(true, HttpStatus.BAD_REQUEST, MESSAGES.ERROR);
      }
    } catch (error) {
      let errorMessage = error.message;
      return returnResponse(
        false,
        HttpStatus.INTERNAL_SERVER_ERROR,
        MESSAGES.NETWORK_ERROR,
        errorMessage,
      );
    }
  }

  async findAll(): Promise<IResponse> {
    try {
      const findAll = await this.authRepo.find({
        select: ['id', 'username', 'createdOn', 'updatedOn'],
      });

      console.log('findAll ===> ', findAll);

      if (findAll.length > 0) {
        return returnResponse(true, HttpStatus.OK, MESSAGES.SUCCESS, findAll);
      } else {
        return returnResponse(true, HttpStatus.NOT_FOUND, MESSAGES.NOT_FOUND);
      }
    } catch (error) {
      let errorMessage = error.message;
      return returnResponse(
        false,
        HttpStatus.INTERNAL_SERVER_ERROR,
        MESSAGES.NETWORK_ERROR,
        errorMessage,
      );
    }
  }

  async findOne(id: number): Promise<IResponse> {
    try {
      const findOne = await this.authRepo.find({
        where: { id },
      });

      console.log('findOne ===> ', findOne);

      if (findOne.length > 0) {
        return returnResponse(true, HttpStatus.OK, MESSAGES.SUCCESS, findOne);
      } else {
        return returnResponse(true, HttpStatus.NOT_FOUND, MESSAGES.NOT_FOUND);
      }
    } catch (error) {
      let errorMessage = error.message;
      return returnResponse(
        false,
        HttpStatus.INTERNAL_SERVER_ERROR,
        MESSAGES.NETWORK_ERROR,
        errorMessage,
      );
    }
  }

  async update(id: number, updateAuthDto: UpdateAuthDto): Promise<IResponse> {
    try {
      let body = {
        ...updateAuthDto,
        password: encrypt(updateAuthDto.password),
      };
      console.log('updateAuthDto ===> ', updateAuthDto);
      let updateUser = await this.authRepo.update(id, body);
      console.log('updateUser ===> ', updateUser);
      if (updateUser.affected === 1) {
        return returnResponse(true, HttpStatus.OK, MESSAGES.SUCCESS);
      } else {
        return returnResponse(true, HttpStatus.NO_CONTENT, MESSAGES.ERROR);
      }
    } catch (error) {
      let errorMessage = error.message;
      return returnResponse(
        false,
        HttpStatus.INTERNAL_SERVER_ERROR,
        MESSAGES.NETWORK_ERROR,
        errorMessage,
      );
    }
  }

  async remove(id: number): Promise<IResponse> {
    try {
      let deleteUser = await this.authRepo.delete(id);
      console.log('deleteUser ===> ', deleteUser);
      if (deleteUser.affected === 1) {
        return returnResponse(true, HttpStatus.OK, MESSAGES.SUCCESS);
      } else {
        return returnResponse(true, HttpStatus.NO_CONTENT, MESSAGES.ERROR);
      }
    } catch (error) {
      let errorMessage = error.message;
      return returnResponse(
        false,
        HttpStatus.INTERNAL_SERVER_ERROR,
        MESSAGES.NETWORK_ERROR,
        errorMessage,
      );
    }
  }

  async singin(user: SinginAuthDto) {
    try {
      const findOne = await this.authRepo.find({
        where: { username: user.username },
      });
      console.log('singin ===> ', findOne);
      if (findOne.length > 0) {
        let result = findOne[0];
        let dePassword = decrypt(result.password);
        console.log('dePassword ===> ', dePassword);

        if (dePassword.toString() === user.password) {
          const payload = {
            userInfo: {
              id: result.id,
              username: result.username,
              createdOn: result.createdOn,
              updatedOn: result.updatedOn,
            },
          };
          return {
            access_token: this.jwtService.sign(payload),
          };
        } else {
          return returnResponse(
            true,
            HttpStatus.NOT_FOUND,
            MESSAGES.ERROR_PASSWORD,
          );
        }
      } else {
        return returnResponse(true, HttpStatus.NOT_FOUND, MESSAGES.ERROR_USER);
      }
    } catch (error) {
      let errorMessage = error.message;
      return returnResponse(
        false,
        HttpStatus.INTERNAL_SERVER_ERROR,
        MESSAGES.NETWORK_ERROR,
        errorMessage,
      );
    }
  }
}

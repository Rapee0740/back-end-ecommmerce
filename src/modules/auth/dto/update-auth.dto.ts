import { IsNotEmpty } from 'class-validator';
import { ApiPropertyOptional } from '@nestjs/swagger';

export class UpdateAuthDto {
  @IsNotEmpty()
  @ApiPropertyOptional({ type: String, example: '123456' })
  password: string;
}

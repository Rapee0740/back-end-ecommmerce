import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class SingupAuthDto {
  @IsNotEmpty()
  @ApiPropertyOptional({ type: String, example: 'admin@gmail.com' })
  username: string;

  @IsNotEmpty()
  @ApiPropertyOptional({ type: String, example: '123456' })
  password: string;
}

import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @ApiPropertyOptional({ type: String, example: 'กางเกง' })
  p_name: string;

  @IsNotEmpty()
  @ApiPropertyOptional({ type: String, example: 'type' })
  p_type: string;

  @IsNotEmpty()
  @ApiPropertyOptional({ type: String, example: 'ดำ' })
  p_color: string;

  @IsNotEmpty()
  @ApiPropertyOptional({ type: String, example: '255' })
  p_price: string;

  @IsNotEmpty()
  @ApiPropertyOptional({ type: String, example: 'xl' })
  p_size: string;

  @ApiPropertyOptional({ type: String, example: 'ขาย' })
  p_status: string;

  @IsNotEmpty()
  @ApiPropertyOptional({ type: String, example: 'description' })
  p_description: string;

  @ApiPropertyOptional({
    required: false,
    type: 'array',
    items: { type: 'file' },
  })
  files?: Array<Express.Multer.File>;
}

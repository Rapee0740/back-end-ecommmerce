import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseInterceptors,
  UploadedFiles,
  UseGuards,
} from '@nestjs/common';
import { ProductsService } from './products.service';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { ApiBearerAuth, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { IResponse } from 'src/interfaces/type-response';
import { FilesInterceptor } from '@nestjs/platform-express';
import { JwtAuthGuard } from 'src/constants/jwt/jwt-auth.guard';

@ApiTags('PRODUCTS')
@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}

  @Post()
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FilesInterceptor('files', 10))
  create(
    @UploadedFiles() files: Array<Express.Multer.File>,
    @Body() body: CreateProductDto,
  ): Promise<IResponse> {
    return this.productsService.create(files, body);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get()
  findAll(): Promise<IResponse> {
    return this.productsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<IResponse> {
    return this.productsService.findOne(id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() body: UpdateProductDto,
  ): Promise<IResponse> {
    return this.productsService.update(+id, body);
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<IResponse> {
    return this.productsService.remove(+id);
  }
}

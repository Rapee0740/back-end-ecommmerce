import { HttpStatus, Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { ProductEntity } from './entities/product.entity';
import { Repository } from 'typeorm';
import { IResponse } from 'src/interfaces/type-response';
import { returnResponse } from 'src/shared/return-response';
import { MESSAGES } from 'src/constants/message';
import { ProductImageEntity } from './entities/image.entity';
import { gen_uuid } from 'src/shared';
import { ENV } from 'src/env/env';
import * as fs from 'fs';
import { SizeEntity } from './entities/size.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(ProductEntity)
    private readonly productRepo: Repository<ProductEntity>,

    @InjectRepository(ProductImageEntity)
    private readonly imageRepo: Repository<ProductImageEntity>,

    @InjectRepository(SizeEntity)
    private readonly sizeRepo: Repository<SizeEntity>,
  ) {}

  async create(
    files: Array<Express.Multer.File>,
    body: CreateProductDto,
  ): Promise<IResponse> {
    try {
      const productModel = new ProductEntity();

      productModel.p_name = body.p_name;
      productModel.p_type = body.p_type;
      productModel.p_color = body.p_color;
      productModel.p_price = body.p_price;
      productModel.p_size = body.p_size;
      productModel.p_status = body.p_status;
      productModel.p_description = body.p_description;

      const saveProduct = await this.productRepo.save(productModel);

      if (saveProduct) {
        for (const item of files) {
          const img_path = `${gen_uuid().toString()}.${
            item.mimetype.split('/')[1]
          }`;

          fs.writeFileSync('./files/' + img_path, item.buffer);

          const imageModal = new ProductImageEntity();
          imageModal.image_name = img_path;
          imageModal.image_type = item.mimetype.split('/')[1];
          imageModal.image_url = ENV().URL_API + img_path;
          imageModal.product = saveProduct;

          await this.imageRepo.save(imageModal);
        }

        return returnResponse(true, HttpStatus.CREATED, MESSAGES.SUCCESS);
      } else {
        return returnResponse(true, HttpStatus.BAD_REQUEST, MESSAGES.ERROR);
      }
    } catch (error) {
      return returnResponse(
        true,
        HttpStatus.INTERNAL_SERVER_ERROR,
        MESSAGES.NETWORK_ERROR,
        error,
      );
    }
  }

  async findAll(): Promise<IResponse> {
    try {
      const findAll = await this.productRepo.find({
        relations: { file: true },
      });

      return returnResponse(
        true,
        HttpStatus.CREATED,
        MESSAGES.SUCCESS,
        findAll,
      );
    } catch (error) {
      return returnResponse(
        true,
        HttpStatus.INTERNAL_SERVER_ERROR,
        MESSAGES.NETWORK_ERROR,
        error,
      );
    }
  }

  async findOne(id: string): Promise<IResponse> {
    try {
      const findOne = await this.productRepo.find({
        where: { id },
        relations: { file: true },
      });

      return returnResponse(true, HttpStatus.OK, MESSAGES.SUCCESS, findOne);
    } catch (error) {
      return returnResponse(
        true,
        HttpStatus.INTERNAL_SERVER_ERROR,
        MESSAGES.NETWORK_ERROR,
        error,
      );
    }
  }

  async update(id: number, body: UpdateProductDto): Promise<IResponse> {
    try {
      const { files, ...result } = body;
      await this.productRepo.update(id, result);

      return returnResponse(true, HttpStatus.OK, MESSAGES.SUCCESS);
    } catch (error) {
      return returnResponse(
        true,
        HttpStatus.INTERNAL_SERVER_ERROR,
        MESSAGES.NETWORK_ERROR,
        error,
      );
    }
  }

  async remove(id: number): Promise<IResponse> {
    try {
      await this.productRepo.delete(id);
      return returnResponse(true, HttpStatus.OK, MESSAGES.SUCCESS);
    } catch (error) {
      return returnResponse(
        true,
        HttpStatus.INTERNAL_SERVER_ERROR,
        MESSAGES.NETWORK_ERROR,
        error,
      );
    }
  }
}

import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ProductImageEntity } from './image.entity';

@Entity('product')
export class ProductEntity {
  @PrimaryGeneratedColumn() id?: string;
  @Column({ type: 'varchar', nullable: false }) p_name: string;
  @Column({ type: 'varchar', nullable: false }) p_type: string;
  @Column({ type: 'varchar', nullable: false }) p_color: string;
  @Column({ type: 'varchar', nullable: false }) p_price: string;
  @Column({ type: 'varchar', nullable: false }) p_size: string;
  @Column({ type: 'varchar', nullable: false }) p_status: string;
  @Column({ type: 'varchar', nullable: false }) p_description: string;

  @OneToMany(() => ProductImageEntity, (Column) => Column.product)
  file?: ProductImageEntity[];

  @CreateDateColumn() createdOn?: Date;
  @CreateDateColumn() updatedOn?: Date;
}

import { BeforeInsert, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('size')
export class SizeEntity {
  @PrimaryGeneratedColumn() id?: string;
  @Column({ type: 'varchar', nullable: false }) size: string;
}

import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ProductEntity } from './product.entity';

@Entity('product-image')
export class ProductImageEntity {
  @PrimaryGeneratedColumn() id?: string;
  @Column({ type: 'varchar', nullable: false }) image_name: string;
  @Column({ type: 'varchar', nullable: false }) image_type: string;
  @Column({ type: 'varchar', nullable: false }) image_url: string;

  @ManyToOne(() => ProductEntity, (Column) => Column.file)
  product?: ProductEntity;

  @CreateDateColumn() createdOn?: Date;
  @CreateDateColumn() updatedOn?: Date;
}

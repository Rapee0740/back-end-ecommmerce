import { Module } from '@nestjs/common';
import { ProductsService } from './products.service';
import { ProductsController } from './products.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductEntity } from './entities/product.entity';
import { ProductImageEntity } from './entities/image.entity';
import { SizeEntity } from './entities/size.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([ProductEntity, ProductImageEntity, SizeEntity]),
  ],
  controllers: [ProductsController],
  providers: [ProductsService],
})
export class ProductsModule {}

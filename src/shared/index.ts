import * as CryptoJS from 'crypto-js';
const keyCrypto =
  '34035c714784ad0a3aeb64afed955c9a2f0cbe04122795724eec65f4c4589e3b';

export const gen_uuid = () => {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = (Math.random() * 16) | 0,
      v = c == 'x' ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
};

export const encrypt = (password: string) => {
  const setType = JSON.stringify(password);
  return CryptoJS.AES.encrypt(setType, keyCrypto).toString();
};

export const decrypt = (password: string) => {
  try {
    var bytes = CryptoJS.AES.decrypt(password, keyCrypto);
    var originalText = bytes.toString(CryptoJS.enc.Utf8);
    return JSON.parse(originalText);
  } catch (error) {
    console.log(error);
    return password;
  }
};

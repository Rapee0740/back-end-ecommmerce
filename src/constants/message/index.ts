export const MESSAGES = {
  SUCCESS: 'ดำเนินการสำเร็จ',
  ERROR: 'ดำเนินการไม่สำเร็จ',
  NETWORK_ERROR: 'ไม่สามารถเชื่อมต่อได้',
  NOT_FOUND: 'ไม่พบข้อมูล',
  ERROR_USER: 'อีเมลไม่ถูกต้อง',
  ERROR_PASSWORD: 'รหัสผ่านไม่ถูกต้อง',
};

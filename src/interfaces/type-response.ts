export interface IResponse {
  result: boolean;
  status: number;
  message: string;
  data?: any;
  error?: any;
  [key: string]: any;
}
